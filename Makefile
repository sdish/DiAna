CC = g++

FLAGS   = -std=c++11 -Wall -Wextra -Werror
OBJ_DIR = obj
OBJ     = $(OBJ_DIR)/*

all: read_file_functions.o
	$(CC) $(FLAGS) src/main.cc $(OBJ) -o DiAna

read_file_functions.o: src/read_file_functions.cc src/read_file_functions.h
	mkdir -p $(OBJ_DIR)
	$(CC) $(FLAGS) -c src/read_file_functions.cc -o obj/read_file_functions.o

clean:
	rm DiAna
	rm -r $(OBJ_DIR)
