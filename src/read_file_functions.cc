#include "read_file_functions.h"

using namespace std;

void readFile(ifstream& FILE) {
	string line;
	int number_of_lines(0);
	int number_of_columns(0);
	while(getline(FILE,line)) {
		int data_number(0);
		if (number_of_lines == 1) {
			istringstream s(line);
			string entry;
			while(getline(s,entry,',')) {
				number_of_columns++;
			}
		}
		istringstream s_line(line);
		string entry;
		while(getline(s_line, entry,',')) {
			data_number++;
			if (!entry.empty()) {
				//const int bg_date(data_number / COLUMNS_IN_DAY + 1);
				istringstream s_entry(entry);
				double double_entry;
				s_entry >> double_entry;
				cout << double_entry << endl;
			}
		}
	}
}
